RT2Jira Userscript
=======

About
-----------

I created this script to help me intergrate RT ticketing system and Jira issue system.
It allows you to create a Jira issue from a tickets Display page. When you click the
"Create Jira Issue" button it will fetch the projects/boards, users, issue types, and
priorities from Jira as well as fetching the the ticket title, and creation body from
RT.

RT2Jira relies on the user already being signed in to both applications and for the
defaults to be configured. This way the script can rely on cookie based
authentication.

Dependecies
-----------

Since this is a [userscript](http://wiki.greasespot.net/User_script), you will need either [Grease Monkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) if you use
Firefox and [Tamper Monkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en) if you use Chromium/Chrome.

Install
-----------

With Grease Monkey/Tamper Monkey installed and enabled, click [INSTALL.](https://bitbucket.org/btreecat/rt2jira/raw/production/rt2jira.user.js)

Configure
-----------

You may need to edit the user script before it is fully functional. Also you may prefer different defaults.

Settings of interest:

* Include URL (Tells the script what page it should be run on)
* RT URL
* Jira URL

The userscript include setting:

    // @include     https://tickets.vtti.vt.edu/Ticket/Display.html*

The variables that need to be edited at the top of the user script:

    //============= Config Variables ======================
    window.jira = "https://jira.vtti.vt.edu/jira/";
    window.rt = "https://tickets.vtti.vt.edu/REST/1.0/ticket/";

Usage
-----------

After installing the plugin and user script, make sure you are logged in to both RT and Jira. Then from the over view page of any ticket, click the "Create Jira Issue" button that is now hovering at the top. Adjust any values as needed before submitting. Drop downs are saved after they are changed. You can hit the [Esc] key to cancel the Jira issue creation.

After clicking submit, you will be asked if you want to open a tab with the new Jira issue in it. The RT ticket has been updated to link to the Jira issue and vice versa. 
