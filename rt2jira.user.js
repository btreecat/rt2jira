// ==UserScript==
// @name        RT2Jira
// @namespace   stephentanner.net
// @description This is used to take a ticket from RT and export it to Jira.
// @include     https://tickets.vtti.vt.edu/Ticket/Display.html*
// @require     http://zeptojs.com/zepto.min.js
// @require     https://raw.github.com/madrobby/zepto/master/src/callbacks.js
// @require     https://raw.github.com/madrobby/zepto/master/src/deferred.js
// @resource    grid https://raw.github.com/nathansmith/unsemantic/master/assets/stylesheets/unsemantic-grid-responsive.css
// @version     1.2.4
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_xmlhttpRequest
// @grant       GM_getResourceText
// @grant       GM_addStyle
// ==/UserScript==

// ============= Config Variables ======================
window.jira = "https://jira.vtti.vt.edu/jira/";
window.rt = "https://tickets.vtti.vt.edu/REST/1.0/ticket/";

//============== START OF MAIN SCRIPT ==================

//Functions baby (Really, I don't like that there is no way to define a clear
//start. So this is clearly where it starts. Right here. Whith the
//call to the main() function. Which is right above this. So don't compmain
//about not being able to find nothing.
main();

//=========== HELPER FUNCTIONS ================

//Persist dropdown selections
function persist_select(){
    Zepto("#jira_issue_types").change(function(event){
        GM_setValue("default_issue_type", $(event.target).val());
    });

    Zepto("#jira_priorities").change(function(event){
        GM_setValue("default_priority", $(event.target).val());
    });

    Zepto("#jira_boards").change(function(event){
        GM_setValue("default_board", $(event.target).val());
    });
}


//Fetch the Project/Board keys
function get_boards(){
    var url = window.jira + "rest/api/2/project";
    var boards;
    var deferred = Zepto.Deferred();

    window.boards = [];

    GM_xmlhttpRequest({
        method: "get",
        url: url,
        onload: function(response) {
            boards = JSON.parse(response.responseText);
            Zepto.each(boards, function(index) {
                window.boards.push(this.key);
            });
            deferred.resolve(response);
        },
        onerror: function(e) {
            console.log(e);
            deferred.reject();
        }
    });
    return deferred.promise();
}

//Fetch the users
function get_users(){
    var keys = "";
    var deferred = Zepto.Deferred();
    for (key in window.boards) {
        keys+= window.boards[key] + ','
    }
    var url = window.jira + "rest/api/2/user/assignable/multiProjectSearch?projectKeys=" + keys;
    var users;

    window.users = [];

    GM_xmlhttpRequest({
        method: "get",
        url: url,
        onload: function(response) {
            users = JSON.parse(response.responseText);
            Zepto.each(users, function(index) {
                window.users.push(this.key);
            });
            deferred.resolve(response);
        },
        onerror: function(e) {
            console.log(e);
            deferred.reject();
        }
    });

    return deferred.promise();
}

//Fetch the priorities
function get_priorities(){
    var url = window.jira + "rest/api/2/priority";
    var priorities;
    var deferred = Zepto.Deferred();

    window.priorities = [];

    GM_xmlhttpRequest({
        method: "get",
        url: url,
        onload: function(response) {
            priorities = JSON.parse(response.responseText);
            Zepto.each(priorities, function(index) {
                window.priorities.push(this.name);
            });
            deferred.resolve(response);
        },
        onerror: function(e) {
            console.log(e);
        }
    });
    return deferred.promise();
}

//Fetch the Issue types
function get_issue_types(){
    var url = window.jira + "rest/api/2/issuetype";
    var issue_types;
    var deferred = Zepto.Deferred();

    window.issue_types = [];

    GM_xmlhttpRequest({
        method: "get",
        url: url,
        onload: function(response) {
            issue_types = JSON.parse(response.responseText);
            Zepto.each(issue_types, function(index) {
                window.issue_types.push(this.name);
            });
            deferred.resolve(response);
        },
        onerror: function(e) {
            console.log(e);
        }
    });
    return deferred.promise();
}


//Build the Top bar
function build_bar() {
    Zepto("div#quickbar").append('<div '
        +'class="grid-container"><div class="grid-20 prefix-30" style="position: fixed; z-index: 10010;">'
        +'<button id="create_issue">Create Jira Issue</button>'
        +'</div></div>');

}

//After the modal is setup, populate it with a form
//and set the values to the ones collected in data.
function build_form(data) {
    var assignees, issue_types, priorities, boards, html;

    var deferred = Zepto.Deferred();

    assignees = build_selectors({opts: window.users, name: 'users', selected: data.fields.assignee.name});

    issue_types = build_selectors({opts: window.issue_types, name: 'issue_types', selected: GM_getValue("default_issue_type")});

    priorities = build_selectors({opts: window.priorities, name: 'priorities', selected: GM_getValue("default_priority")});

    boards = build_selectors({opts: window.boards, name: 'boards', selected: GM_getValue("default_board")});

    html = "<h3>Review Values:</h3>" +
        "<dl id='form_list'>" +
        "<dt><h4>Summary:</h4></dt><dd><input id='jira_summary' type='text' value='" + data.fields.summary +  "'/></dd>" +
        "<dt><h4>Assignee:</h4></dt><dd>" + assignees + "</dd>" +
        "<dt><h4>Issue Type:</h4></dt><dd>" + issue_types + "</dd>" +
        "<dt><h4>Priority:</h4></dt><dd>" + priorities + "</dd>" +
        "<dt><h4>Board:</h4></dt><dd>" + boards + "</dd>" +
        "<dt><h4>Description:</h4></dt><dd><textarea id='jira_description' rows='10' cols='42' style='white-space: nowrap;'>" + data.fields.description + "</textarea></dd>" +
        "<br /><button id='sub_issue'>Submit Issue</button>" +
        "</dl>";

    Zepto("#form_box").html(html);

    deferred.resolve(data);
    return deferred.promise();
}

//Need a function to generate the 4 selectors
//This Will generate a selector given the values passed
function build_selectors(options) {
    var sel_string = "<select name='" + options.name + "' id='jira_" + options.name + "'>"
    var opts = build_options(options.opts, options.selected);
    for (opt in opts) {
        sel_string+=opts[opt];
    }

    sel_string += "</select>";

    return sel_string;

}

//This function will be a helper function for the selector builder.
//Here we will take an array of strings and build an option list
function build_options(options, selected) {
    var opts = [];
    for (opt in options) {
        if (options[opt] == selected) {
            var op = "<option value='" + options[opt] + "' selected>" + options[opt] + "</option>";
            opts.push(op);
        }
        else {
            var op = "<option value='" + options[opt] + "'>" + options[opt] + "</option>";
            opts.push(op);
        }

    }
    return opts;
}

//build the modal to allow preview/edit of info
function build_modal() {
    var body = Zepto("body");
    body.append('<div class="overlay" style="background-color: #000; opacity: .7; filter: alpha(opacity=70); position: fixed; top: 0; left: 0; width: 100%; height: 100%; z-index: 10000;"></div>');
    body.append('<div class="overlay grid-container grid-30 prefix-35" style="position: fixed; top: 10%; left: 0; z-index: 10001; background: white"><div id="form_box" class="grid-50 prefix-5">Building JIRA Issue form...</div></div>')

    //Destructor
    Zepto(document).on('keyup', function(e){
        //user hit escape
        if (e.keyCode == 27) {
            Zepto(".overlay").remove();
        }
    });
}

//This method will post the JIRA ticket url to the RT "refers to" field
function link_back(url) {
    var data, link;
    var deferred = Zepto.Deferred();

    link = "RefersTo: " + url;
    data = {"content": link}

    Zepto.ajax({
        type: "POST",
        url: window.rt + window.ticket_num + "/links",
        data: data,
        success: function (response) {
            console.log(response);
            deferred.resolve(response);
        }

    });
    return deferred.promise();
}

//This method will post the RT url to the JIRA issue now that it has been created
function link_forward(url, issue) {
    var data;
    var deferred = Zepto.Deferred();

    data = {object: {
        url: url,
        title: "Ticket #" + url.split('=')[1]
    }
    };

    GM_xmlhttpRequest({
        method: "POST",
        url: window.jira + "rest/api/2/issue/" + issue.key + "/remotelink",
        headers: {
            "Content-Type" : "application/json"
        },
        data: JSON.stringify(data),
        onload: function(response) {
            deferred.resolve(response);
        },
        onerror: function(response) {
            console.log("The server returned an error.");
            console.log(response);
            deferred.reject();
        }
    });
    return deferred.promise();
}


//Create the issue by taking a data object, converting it to a string
//and posting it to Jira.
//Jira should respond with JSON data back including an issue ID and url.
function create_issue(data) {
    var issue;
    var deferred = Zepto.Deferred();
    GM_xmlhttpRequest({
        method: "POST",
        url: window.jira + "rest/api/2/issue/",
        headers: {
            "Content-Type" : "application/json"
        },
        data: JSON.stringify(data),
        onload: function(response) {
            deferred.resolve(response);
        },
        onerror: function(response) {
            console.log("The server returned an error.");
            console.log(response);
            deferred.reject();
        }

    });
    return deferred.promise();
}

//Fetch the main ticket values from the RT REST API
function get_rt_ticket() {
    var deferred = Zepto.Deferred();

    Zepto.ajax({
        type: "GET",
        url: window.rt + window.ticket_num + "/show",
        dataType: "text",
        async: false,
        success: function (reply) {
            var lines, row, ticket;

            ticket = {};

            lines = reply.split("\n");
            Zepto.each(lines, function(index, value) {
                var key, val;

                row = value.split(/: ?/);
                key = row[0];
                val = row[1];
                if (key.search(/^RT\/\d/) == -1 && key != '') {
                    ticket[key.toLowerCase()] = val;
                }
            });
            deferred.resolve(ticket);
        }
    });
    return deferred.promise();
}

//Fetch the description and remaining comments from RT REST API
function get_rt_history() {
    var deferred = Zepto.Deferred();

    Zepto.ajax({
        type: "GET",
        url: window.rt + window.ticket_num + "/history?format=l",
        dataType: "text",
        success: function (reply) {
            var comments, history;
            history = [];
            comments = reply.split(/^--$/gm);

            Zepto.each(comments, function(index, value) {
                var keys, vals, comment;
                comment = {};
                vals = value.split(/^\w+: ?/gm);
                //The selector is not perfect so we end up grabbing the REST
                //header so we discard the first element to make the arrays
                //line up allowing us to build our comments more easily.
                vals.shift();
                keys = value.match(/^\w+ ?/gm);

                //Ok now we have captured the key-val maps for
                //the individual comment
                Zepto.each(keys, function(i, k) {
                    comment[k.toLowerCase()] = vals[i-1];
                });
                history.push(comment);
            });

            deferred.resolve(history);
        }
    });
    return deferred.promise();
}

//Fetch all the values on a page and return a data object
function build_ticket() {
    var data = {};

    var deferred = Zepto.Deferred();

    window.ticket_num = Zepto(".id .value").text();

    data.fields = {};

    var rt_head = get_rt_ticket();

    var rt_history = get_rt_history();

    rt_head.done(function(ticket){

        rt_history.done(function(history) {
            data.fields.project = {key: ""};
            data.fields.issuetype = {name: ""};
            data.fields.priority = {name: ""};

            data.fields.summary = ticket.subject;
            data.fields.description = history[0].content;

            data.fields.assignee = {name: ticket.owner};

            deferred.resolve(data);
        });

    });

    return deferred.promise();
}

//Handle the control logic of what to do after a ticket has been sucessfully
//created.
function response_handler(response) {
    //BOOSH!
    if (response.status == 201) {
        var url, issue;

        issue = JSON.parse(response.responseText);
//        console.log(issue);
        url = window.jira + "browse/" + issue.key;
        link_back(url);
        link_forward(document.URL, issue);
        if(confirm("Issue has been sucessfully added to JIRA. Click OK to open the issue in JIRA.")) {
            window.open(url ,'_blank');
        }

    }
    //DANGER WILL ROBINSON!
    else if (response.status == 400) {
        console.log("There was an error creating the JIRA task.");
        console.log(response);
    }
    //WTF just happened?
    else {
        console.log("The server returned some status code that I was not expecting.");
        console.log(response);
    }

}

//This method coordinates submitting the ticket
function submit_handler(data) {
    data.fields.project.key = Zepto("#jira_boards").val();
    data.fields.issuetype.name = Zepto("#jira_issue_types").val();
    data.fields.priority.name = Zepto("#jira_priorities").val();
    data.fields.assignee.name = Zepto("#jira_users").val();

    data.fields.summary = Zepto("#jira_summary").val();
    data.fields.description = Zepto("#jira_description").val();

    //    Fire the creation event
    var issue = create_issue(data);
    issue.done(function(response){
        Zepto(".overlay").remove();
        response_handler(response);
    });
}


//This function is used when the "create jira issue" button is clicked
function button_action() {
    var boards, users, issue_types, priorities, ticket, form;

    //init the modal
    build_modal();

    issue_types = get_issue_types();
    priorities = get_priorities();
    ticket = build_ticket();

    //get all the info to POST the ticket
    boards = get_boards();
    boards.done(function(){
        users = get_users();
        users.done(function(){
            issue_types.done(function(){
                priorities.done(function(){
                    ticket.done(function(data) {
                        //populate modal with form data
                        form = build_form(data);
                        form.done(function(data){
                            persist_select();

                            Zepto('#sub_issue').click(function(){
                                submit_handler(data);

                            });
                        });
                    });
                });
            });
        });
    });
    //Should have used a promise lib that includes a "when" method to chain all these damn events together.
}


//This is the main control function. This is what starts everything off.
function main() {
    var grid = GM_getResourceText("grid");
    GM_addStyle(grid);

    window.boards = [];
    window.issue_types = [];
    window.priorities = [];
    window.users = [];

    build_bar();

    Zepto("#create_issue").click(function() {

        var overlay = Zepto(".overlay");

        if (overlay.length) {
            Zepto(".overlay").remove();
        }
        else {
            button_action();
        }
    });
}
